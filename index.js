import React, { useState } from "react";
import CatalogCard from "./CatalogCard";
import ForwardArrow from "./icons/Forward";
import BackdArrow from "./icons/Backward";
import {
  Wrapper,
  TabsBox,
  Tab,
  Divider,
  ChallengeBox,
  ChallengeSlider,
  CustomSlider
} from "./CatalogTabsStyles";
import './react-slick.css'


const CatalogTabs = ({ catalogTabs, catalog, onTabChange }) => {
  const [carouselIndex, setCarouselIndex] = useState(0);
  const settings = {
    dots: false,
    infinite: false,
    speed: 400,
    slidesToShow: 3.2,
    slidesToScroll: 1,
    arrows: true,
    afterChange: index => setCarouselIndex(index),
    nextArrow: <ForwardArrow carouselIndex={carouselIndex} />,
    prevArrow: <BackdArrow carouselIndex={carouselIndex} />,
    responsive: [
      {
        breakpoint: 1441,
        settings: {
          slidesToShow: 2.2
        }
      }
    ]
  };

  const catalogController = (catalog, category) => {
    const result = catalog.filter(catalog => catalog.category === category);
    if (result.length === 0) return false;
    else return true;
  };

  return (
    <Wrapper>
      <TabsBox>
        {catalogTabs.map(tab => (
          <React.Fragment key={tab.id}>
            <Tab selected={tab.selected} onClick={() => onTabChange(tab.id)}>
              <p>{tab.name}</p>
            </Tab>
            <Divider />
          </React.Fragment>
        ))}
      </TabsBox>
      {catalogTabs[0].selected === true && (
        <>
          {/* some repetition here, can't solve how to reduce */}
          {catalogController(catalog, "front-end") && (
            <ChallengeBox>
              <p>{catalogTabs[1].name}</p>
              <ChallengeSlider>
                <CustomSlider {...settings}>
                  {catalog
                    .filter(catalog => catalog.category === "front-end")
                    .map(catalog => (
                      <CatalogCard key={catalog.id} catalog={catalog} />
                    ))}
                </CustomSlider>
              </ChallengeSlider>
            </ChallengeBox>
          )}

          {catalogController(catalog, "full-stack") && (
            <ChallengeBox>
              <p>{catalogTabs[2].name}</p>
              <ChallengeSlider>
                <CustomSlider {...settings}>
                  {catalog
                    .filter(catalog => catalog.category === "full-stack")
                    .map(catalog => (
                      <CatalogCard key={catalog.id} catalog={catalog} />
                    ))}
                </CustomSlider>
              </ChallengeSlider>
            </ChallengeBox>
          )}

          {catalogController(catalog, "back-end") && (
            <ChallengeBox>
              <p>{catalogTabs[3].name}</p>
              <ChallengeSlider>
                <CustomSlider {...settings}>
                  {catalog
                    .filter(catalog => catalog.category === "back-end")
                    .map(catalog => (
                      <CatalogCard key={catalog.id} catalog={catalog} />
                    ))}
                </CustomSlider>
              </ChallengeSlider>
            </ChallengeBox>
          )}
        </>
      )}
      {catalogTabs[1].selected === true && (
        <ChallengeBox>
          <p>{catalogTabs[1].name}</p>
          <ChallengeSlider catalogTabs={catalogTabs}>
            {catalog
              .filter(catalog => catalog.category === "front-end")
              .map(catalog => (
                <CatalogCard key={catalog.id} catalog={catalog} />
              ))}
          </ChallengeSlider>
        </ChallengeBox>
      )}
      {catalogTabs[2].selected === true && (
        <ChallengeBox>
          <p>{catalogTabs[2].name}</p>
          <ChallengeSlider catalogTabs={catalogTabs}>
            {catalog
              .filter(catalog => catalog.category === "full-stack")
              .map(catalog => (
                <CatalogCard key={catalog.id} catalog={catalog} />
              ))}
          </ChallengeSlider>
        </ChallengeBox>
      )}
      {catalogTabs[3].selected === true && (
        <ChallengeBox>
          <p>{catalogTabs[3].name}</p>
          <ChallengeSlider catalogTabs={catalogTabs}>
            {catalog
              .filter(catalog => catalog.category === "back-end")
              .map(catalog => (
                <CatalogCard key={catalog.id} catalog={catalog} />
              ))}
          </ChallengeSlider>
        </ChallengeBox>
      )}
    </Wrapper>
  );
};

export default CatalogTabs;